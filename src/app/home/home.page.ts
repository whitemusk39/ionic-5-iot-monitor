import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth/auth.service';
import { TopicService } from '../services/topic/topic.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  topics = [];

  constructor(
    private router: Router,
    private navController: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private authService: AuthService,
    private topicService: TopicService,
  ) {}

  async ngOnInit() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.topicService
      .getTopics()
      .subscribe((response) => {
        this.topics = response;
        loading.dismiss();
      }, async (error) => {
        loading.dismiss();

        const alert = await this.alertController.create({
          header: 'Loading Topics Failed',
          message: error.message,
          buttons: ['OK']
        });

        await alert.present();
      });
  }

  async addTopicPrompt() {
    const alert = await this.alertController.create({
      header: 'Add Topic',
      inputs: [
        {
          name: 'topic',
          type: 'text',
          placeholder: 'Topic Name',
          attributes: {
            required: true
          }
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'OK',
          handler: async (data) => {
            const loading = await this.loadingController.create();
            await loading.present();
            
            this.topicService.addTopic(data)
              .then(async () => {
                loading.dismiss();
              }, async (error) => {
                loading.dismiss();
        
                const alert = await this.alertController.create({
                  header: 'Add Topic Failed',
                  message: error.message,
                  buttons: ['OK']
                });
        
                await alert.present();
              });
          }
        }
      ]
    });

    await alert.present();
  }

  async editTopicPrompt(topic) {
    const alert = await this.alertController.create({
      header: 'Edit Topic',
      inputs: [
        {
          name: 'topic',
          type: 'text',
          placeholder: 'Topic Name',
          value: topic.topic
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'OK',
          handler: async (data) => {
            const loading = await this.loadingController.create();
            await loading.present();

            this.topicService.updateTopic({ id: topic.id, topic: data.topic })
              .then(async () => {
                loading.dismiss();
              }, async (error) => {
                loading.dismiss();
        
                const alert = await this.alertController.create({
                  header: 'Add Topic Failed',
                  message: error.message,
                  buttons: ['OK']
                });
        
                await alert.present();
              });
          }
        }
      ]
    });

    await alert.present();
  }

  async deleteTopic(id) {
    const alert = await this.alertController.create({
      header: 'Confirm delete?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'Yes',
          handler: async() => {
            const loading = await this.loadingController.create();
            await loading.present();

            this.topicService
              .deleteTopic(id)
              .then(async (response) => {
                loading.dismiss();

                const alert = await this.alertController.create({
                  header: 'Topic Deleted',
                  buttons: ['OK']
                });

                await alert.present();
              }, async (error) => {
                loading.dismiss();

                const alert = await this.alertController.create({
                  header: 'Loading Topics Failed',
                  message: error.message,
                  buttons: ['OK']
                });

                await alert.present();
              });
          }
        }
      ]
    });

    await alert.present();
  }

  viewTopic(topic) {
    const params = Object.assign({}, topic);
    this.navController.navigateForward('/view-topic', { queryParams: params });
  }

  async logout() {
    const alert = await this.alertController.create({
      header: 'Confirm logout?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'Yes',
          handler: async () => {
            const loading = await this.loadingController.create();
            loading.present();

            this.authService.logout()
              .then(() => {
                loading.dismiss();
                this.router.navigateByUrl('/', { replaceUrl: true });
              });
          }
        }
      ]
    });

    await alert.present();
  }

}
