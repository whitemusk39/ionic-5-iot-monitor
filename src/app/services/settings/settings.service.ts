import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';

export interface Settings {
  id: string;
  hostname: string;
  port: number;
  path: string;
  createdAt: firebase.default.firestore.FieldValue;
};

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  private _settings: Settings = null;

  constructor() { }

  save(data) {
    
  }
}
