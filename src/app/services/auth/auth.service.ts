import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

export interface User {
  uid: string;
  email: string;
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser: User = null;

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) {
    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
    });
  }

  async register({email, password}): Promise<any> {
    const credential = await this.afAuth.createUserWithEmailAndPassword(email, password);

    return this.afs
      .doc(`users/$uid`)
      .set({
        uid: credential.user.uid,
        email: credential.user.email
      });
  }

  login({email, password}) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  logout(): Promise<void> {
    return this.afAuth.signOut();
  }
}
