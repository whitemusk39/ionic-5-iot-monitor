import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';

export interface Topic {
  id: string;
  topic: string;
  createdAt: firebase.default.firestore.FieldValue;
};

@Injectable({
  providedIn: 'root'
})
export class TopicService {
  private _currentTopic: Topic = null;

  constructor(
    private afs: AngularFirestore,
  ) { }

  addTopic(data) {
    return this.afs.collection('topics').add({
      topic: data.topic,
      createdAt: firebase.default.firestore.FieldValue.serverTimestamp()
    });
  }

  updateTopic(data) {
    return this.afs.collection('topics').doc(data.id)
      .update({
        topic: data.topic
      });
  }

  getTopics() {
    return this.afs.collection('topics', ref => ref.orderBy('createdAt'))
      .valueChanges({ idField: 'id' }) as Observable<Topic[]>;
  }

  getTopic(id) {
    return this.afs.collection('topics').doc(id).get();
  }

  deleteTopic(id) {
    return this.afs.collection('topics').doc(id).delete();
  }

  public set currentTopic(topic) {
    this._currentTopic = topic;
  }

  public get currentTopic() {
    return this._currentTopic;
  }
}
