import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, LoadingController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private navController: NavController,
    private alertController: AlertController,
    private loadingController: LoadingController,
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      hostname: ['broker.emqx.io', [Validators.required]],
      port: ['8083', [Validators.required]],
      path: ['/mqtt', [Validators.required]],
    });
  }

  get hostname() {
    return this.form.get('hostname');
  }

  get port() {
    return this.form.get('port');
  }

  get path() {
    return this.form.get('path');
  }

}
