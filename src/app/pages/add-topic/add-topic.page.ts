import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { TopicService } from 'src/app/services/topic/topic.service';

@Component({
  selector: 'app-add-topic',
  templateUrl: './add-topic.page.html',
  styleUrls: ['./add-topic.page.scss'],
})
export class AddTopicPage implements OnInit {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private navController: NavController,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private topicService: TopicService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      topic: ['', [Validators.required]]
    });
  }

  async save() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.topicService
      .addTopic(this.form.value)
      .then(async () => {
        loading.dismiss();
        this.navController.navigateBack('/home');
      }, async (error) => {
        loading.dismiss();

        const alert = await this.alertController.create({
          header: 'Add Topic Failed',
          message: error.message,
          buttons: ['OK']
        });

        await alert.present();
      });
  }

  get topic() {
    return this.form.get('topic');
  }
}
