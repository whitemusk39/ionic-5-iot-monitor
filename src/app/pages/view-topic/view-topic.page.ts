import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartColor } from '@rinminase/ng-charts';
import { IMqttMessage, MqttService } from 'ngx-mqtt';

@Component({
  selector: 'app-view-topic',
  templateUrl: './view-topic.page.html',
  styleUrls: ['./view-topic.page.scss'],
})
export class ViewTopicPage implements OnInit {
  topic = null;

  // Charts
  chartData = [
    { data: [], label: 'Value' }
  ];

  chartLabels = [];

  chartOptions = {
    responsive: true,
  };

  chartColors: ChartColor = [
    {
      borderColor: 'red',
      backgroundColor: 'rgba(255,0,0,0.3)',
    }
  ];

  chartLegend = true;

  constructor(
    private route: ActivatedRoute,
    private _mqttService: MqttService
  ) {
    this.route.queryParams.subscribe((params) => {
      this.topic = params;

      this._mqttService.observe(this.topic.topic).subscribe((message: IMqttMessage) => {
        this.updateChart(message.payload.toString());
      });
    });
  }

  ngOnInit() {}

  updateChart(value) {
    const now = new Date();
    this.chartLabels.push(`${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`);
    this.chartData[0].data.push(value);
  }
}
