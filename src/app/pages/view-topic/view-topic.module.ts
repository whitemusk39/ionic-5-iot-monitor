import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewTopicPageRoutingModule } from './view-topic-routing.module';

import { ViewTopicPage } from './view-topic.page';
import { ChartsModule } from '@rinminase/ng-charts';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewTopicPageRoutingModule,
    ChartsModule,
  ],
  declarations: [ViewTopicPage]
})
export class ViewTopicPageModule {}
