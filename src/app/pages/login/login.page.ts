import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentialForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private alertController: AlertController,
    private loadingController: LoadingController,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.credentialForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  async register() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.authService
      .register(this.credentialForm.value)
      .then((user) => {
        loading.dismiss();
        this.router.navigateByUrl('/home', { replaceUrl: true });
      }, async (error) => {
        loading.dismiss();

        const alert = await this.alertController.create({
          header: 'Register Failed',
          message: error.message,
          buttons: ['OK']
        });

        await alert.present();
      });
  }

  async login() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.authService
      .login(this.credentialForm.value)
      .then((user) => {
        loading.dismiss();
        this.router.navigateByUrl('/home', { replaceUrl: true });
      }, async (error) => {
        loading.dismiss();

        const alert = await this.alertController.create({
          header: 'Login Failed',
          message: error.message,
          buttons: ['OK']
        });

        await alert.present();
      });
  }

  get email() {
    return this.credentialForm.get('email');
  }

  get password() {
    return this.credentialForm.get('password');
  }

}
