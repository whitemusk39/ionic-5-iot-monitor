// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD1ShvQkp9LYS7eIBxvWAbbwFf-u1D0qEo",
    authDomain: "iotmonitor-ionic.firebaseapp.com",
    databaseURL: "https://iotmonitor-ionic-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "iotmonitor-ionic",
    storageBucket: "iotmonitor-ionic.appspot.com",
    messagingSenderId: "69826038169",
    appId: "1:69826038169:web:82112ec79e44b68b736b08"
  },
  mqttConfig: {
    hostname: 'broker.emqx.io',
    port: 8083,
    path: '/mqtt'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
