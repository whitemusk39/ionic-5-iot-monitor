<!-- Styles -->

<link rel="stylesheet" type="text/css" href="./prism.css">
<link rel="stylesheet" type="text/css" href="./style.css">

<!-- Document -->

# Ionic 5 Simple IOT Monitor With MQTT

_A simple IOT Monitoring mobile application using Ionic 5 + Angular and MQTT (in Websocket)._


**Completed sample:** [https://gitlab.com/whitemusk39/ionic-5-iot-monitor](https://gitlab.com/whitemusk39/ionic-5-iot-monitor)

| | | |
| --- | --- | --- |
| ![Login Page](img/login.png) | ![Home Page](img/home.png) | ![Logout](img/logout.png) |
| ![Add Topic](img/add-topic.png) | ![Edit Topic](img/edit-topic.png) | ![Delete Topic](img/delete-topic.png) |
| ![View Topic](img/view-topic-plotted.png) | | |

## Table of Contents

- [Ionic 5 Simple IOT Monitor With MQTT](#ionic-5-simple-iot-monitor-with-mqtt)
  - [Table of Contents](#table-of-contents)
  - [Tools Required](#tools-required)
  - [1. Install Required Tools](#1-install-required-tools)
    - [i. Install Node JS](#i-install-node-js)
    - [ii. Install Ionic CLI](#ii-install-ionic-cli)
    - [iii. Install Angular CLI](#iii-install-angular-cli)
  - [2. Start A Blank Ionic 5 Project](#2-start-a-blank-ionic-5-project)
    - [i. Creating a blank project](#i-creating-a-blank-project)
    - [ii. Install required modules](#ii-install-required-modules)
  - [3. Creating Login Page](#3-creating-login-page)
    - [i. Generate page](#i-generate-page)
    - [ii. Add `ReactiveFormsModule` to login page.](#ii-add-reactiveformsmodule-to-login-page)
    - [iii. Setup Form and getter functions](#iii-setup-form-and-getter-functions)
    - [iv. Add `register` function.](#iv-add-register-function)
    - [v. Add `login` function.](#v-add-login-function)
    - [vii. Create form in the HTML page](#vii-create-form-in-the-html-page)
    - [viii. Styling input error validation](#viii-styling-input-error-validation)
    - [ix. Modify Router so it redirects to our login page instead of home.](#ix-modify-router-so-it-redirects-to-our-login-page-instead-of-home)
    - [x. How it should look like](#x-how-it-should-look-like)
  - [4. Creating Home Page](#4-creating-home-page)
    - [i. No need to create a new page. We will modify the existing `src/app/home` page.](#i-no-need-to-create-a-new-page-we-will-modify-the-existing-srcapphome-page)
    - [ii. Import required modules for `Home` page.](#ii-import-required-modules-for-home-page)
    - [iii. Create logout function.](#iii-create-logout-function)
    - [iv. Create `Add Topic` function.](#iv-create-add-topic-function)
    - [v. Create `Edit Topic` function.](#v-create-edit-topic-function)
    - [vi. Create `Delete Topic` function.](#vi-create-delete-topic-function)
    - [vi. Create `View Topic` function.](#vi-create-view-topic-function)
    - [vii. Start working on the page design.](#vii-start-working-on-the-page-design)
    - [viii. Add logout button in `ion-header`.](#viii-add-logout-button-in-ion-header)
    - [ix. Replace contents inside `ion-content`.](#ix-replace-contents-inside-ion-content)
    - [x. How it should look like](#x-how-it-should-look-like-1)
  - [5. Create View Topic Page](#5-create-view-topic-page)
    - [i. Generate page](#i-generate-page-1)
    - [ii. Import `ChartsModule` into `view-topic.module.ts`.](#ii-import-chartsmodule-into-view-topicmodulets)
    - [iii. Import required modules into `view-topic.page.ts`.](#iii-import-required-modules-into-view-topicpagets)
    - [iv. Initialise `Topic` object, `Charts` data and get `params` from `router`.](#iv-initialise-topic-object-charts-data-and-get-params-from-router)
    - [v. Create `updateChart()` function](#v-create-updatechart-function)
    - [vi. Add back button to `view-topic.page.html`.](#vi-add-back-button-to-view-topicpagehtml)
    - [vi. Add `Line Chart` to `view-topic.page.html`.](#vi-add-line-chart-to-view-topicpagehtml)
    - [vii. How it should look like](#vii-how-it-should-look-like)
  - [6. Setup Firebase](#6-setup-firebase)
    - [i. Create a Firebase Project](#i-create-a-firebase-project)
    - [ii. Enable `Authentication` provider.](#ii-enable-authentication-provider)
    - [iii. Enable `Firestore Database`.](#iii-enable-firestore-database)
    - [iv. Create Firebase Config for web app.](#iv-create-firebase-config-for-web-app)
    - [v. Add `firebaseConfig` to `src/environments/environment.ts`](#v-add-firebaseconfig-to-srcenvironmentsenvironmentts)
    - [vi. Import `Firebase` and `AngularFire` modules to `app.module.ts`](#vi-import-firebase-and-angularfire-modules-to-appmodulets)
  - [7. Setup MQTT](#7-setup-mqtt)
    - [i. Get an MQTT Broker account/server](#i-get-an-mqtt-broker-accountserver)
    - [ii. Add `mqttConfig` into `environment.ts`](#ii-add-mqttconfig-into-environmentts)
    - [iii. Import `ngx-mqtt` modules to `app.module.ts`](#iii-import-ngx-mqtt-modules-to-appmodulets)
    - [iv. Add `MqttService` into `view-topic.page.ts`](#iv-add-mqttservice-into-view-topicpagets)
  - [8. Create `AuthService` Service](#8-create-authservice-service)
    - [i. Generate service](#i-generate-service)
    - [ii. Import `AngularFireAuth` and `AngularFirestore`](#ii-import-angularfireauth-and-angularfirestore)
    - [iii. Create a `User` interface and let `AngularFireAuth` observe the change](#iii-create-a-user-interface-and-let-angularfireauth-observe-the-change)
    - [iv. Create `register()` function](#iv-create-register-function)
    - [v. Create `login()` function](#v-create-login-function)
    - [vi. Create `logout()` function](#vi-create-logout-function)
    - [vii. Import `AuthService` in `login.page.ts`](#vii-import-authservice-in-loginpagets)
    - [viii. Connect `register()` in `login.page.ts` with `AuthService`](#viii-connect-register-in-loginpagets-with-authservice)
    - [ix. Connect `login()` in `login.page.ts` with `AuthService`](#ix-connect-login-in-loginpagets-with-authservice)
    - [x. Import `AuthService` in `home.page.ts`](#x-import-authservice-in-homepagets)
    - [xi. Connect `logout()` in `home.page.ts` with `AuthService`](#xi-connect-logout-in-homepagets-with-authservice)
  - [9. Create `TopicService` Service](#9-create-topicservice-service)
    - [i. Generate service](#i-generate-service-1)
    - [ii. Import modules](#ii-import-modules)
    - [iii. Create `addTopic()` function](#iii-create-addtopic-function)
    - [iv. Create `updateTopic()` function](#iv-create-updatetopic-function)
    - [v. Create `getTopics()` function](#v-create-gettopics-function)
    - [vi. Create `getTopic()` function](#vi-create-gettopic-function)
    - [vii. Create `deleteTopic()` function](#vii-create-deletetopic-function)
    - [viii. Create getter and setter functions](#viii-create-getter-and-setter-functions)
    - [x. Import `TopicService` into `home.page.ts`](#x-import-topicservice-into-homepagets)
    - [xi. Call `getTopics()` in `ngOnInit()` in `home.page.ts`](#xi-call-gettopics-in-ngoninit-in-homepagets)
    - [xii. Connect `addTopicPrompt()` in `home.page.ts` with `TopicService`](#xii-connect-addtopicprompt-in-homepagets-with-topicservice)
    - [xiii. Connect `editTopicPrompt()` in `home.page.ts` with `TopicService`](#xiii-connect-edittopicprompt-in-homepagets-with-topicservice)
    - [xiv. Connect `deleteTopic()` in `home.page.ts` with `TopicService`](#xiv-connect-deletetopic-in-homepagets-with-topicservice)
    - [xv. Connect `viewTopic()` in `home.page.ts` with `TopicService`](#xv-connect-viewtopic-in-homepagets-with-topicservice)
  - [10. Send Message From MQTT Client](#10-send-message-from-mqtt-client)
    - [i. Create a Topic](#i-create-a-topic)
    - [ii. MQTT WebSocket Toolkit by EMQX](#ii-mqtt-websocket-toolkit-by-emqx)
  - [What's Next For You?](#whats-next-for-you)

<div class="pagebreak"></div>

## Tools Required
| Tool           | Version |
| -------------- | ------- |
| `nodejs`       | `^14.x` |
| `@ionic/cli`   | `^6.x`  |
| `@angular/cli` | `^12.x` |

## 1. Install Required Tools
### i. Install Node JS
Check version with <kbd>node --version</kbd> in terminal.  \
Make sure version is `14.x` for best compatibility.  \
Lower versions are not recommended and will cause issues.

> If not installed:  
> Install Node JS LTS version 14.x from https://nodejs.org/en/download/.


> **IMPORTANT: Make sure to add to `$PATH` to ensure the `node` and `npm` commands are accessible from your terminal and editors.**

### ii. Install Ionic CLI
> `npm install -g @ionic/cli@^6`

### iii. Install Angular CLI
> `npm install -g @angular/cli@^12`

## 2. Start A Blank Ionic 5 Project
### i. Creating a blank project
> `ionic start iotmonitor blank --type=angular --capacitor`  \
> `cd iotmonitor`

### ii. Install required modules
> `ng add @angular/fire`  \
> `npm install @rinminase/ng-charts`  \
> `npm install ngx-mqtt`

<div class="pagebreak"></div>

## 3. Creating Login Page
### i. Generate page
> `ionic g page pages/login`

### ii. Add `ReactiveFormsModule` to login page.
> **_"What is Reactive Form?"_**  \
> Reactive forms provide a model-driven approach to handling form inputs whose values change over time.  \
> \
> See: https://angular.io/guide/reactive-forms

<pre><code class="language-typescript line-numbers"># src/app/pages/login/login.module.ts

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
<mark>import { FormsModule, ReactiveFormsModule } from '@angular/forms';</mark>

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,
<mark>    ReactiveFormsModule</mark>
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
</code></pre>

### iii. Setup Form and getter functions
We are using `FormGroup`, `FormBuilder` and `Validators` provided by Angular to build our form.


`FormGroup` will group all the inputs into one group.  \
`FormBuilder` is to tell the application what input fields will the form have.  \
`Validators` is to create validation rules for our input fields so the users must follow the rules or their input will be invalid and cannot be submitted.

Then, we will also add getter functions for the input fields that we will be making so that it can be accessed easily in the HTML page later on.

<pre><code class="language-typescript line-numbers"># src/app/pages/login.page.ts

import { Component, OnInit } from '@angular/core';
<mark>import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';</mark>

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
<mark>  credentialForm: FormGroup;</mark>

  constructor(
<mark>    private fb: FormBuilder,
    private router: Router,
    private alertController: AlertController,
    private loadingController: LoadingController,</mark>
  ) { }

  ngOnInit() {
<mark>    this.credentialForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });</mark>
  }

<mark>  get email() {
    return this.credentialForm.get('email');
  }</mark>

<mark>  get password() {
    return this.credentialForm.get('password');
  }</mark>
}
</code></pre>

### iv. Add `register` function.
This does nothing yet until we create AuthService.

<pre><code class="language-typescript line-numbers"># src/app/pages/login.page.ts

// ...

ngOnInit() {
  // ...
}

async register() {
  const loading = await this.loadingController.create();
  await loading.present();
  loading.dismiss();
  this.router.navigateByUrl('/home', { replaceUrl: true });

  // TODO: Connect with AuthService
}

// ...
</code></pre>

### v. Add `login` function.
This does nothing yet until we create AuthService.

<pre><code class="language-typescript line-numbers"># src/app/pages/login.page.ts

// ...

async register() {
  // ...
}

async login() {
  const loading = await this.loadingController.create();
  await loading.present();
  loading.dismiss();
  this.router.navigateByUrl('/home', { replaceUrl: true });

  // TODO: Connect with AuthService
}

// ...
</code></pre>

### vii. Create form in the HTML page
<pre><code class="language-html line-numbers"># src/app/pages/login/login.page.html

&lt;ion-header&gt;
	&lt;ion-toolbar&gt;
<mark>		&lt;ion-title&gt;Login&lt;/ion-title&gt;</mark>
	&lt;/ion-toolbar&gt;
&lt;/ion-header&gt;

<mark>&lt;ion-content class=&quot;ion-padding&quot;&gt;
	&lt;form [formGroup]=&quot;credentialForm&quot;&gt;
		&lt;ion-item&gt;
			&lt;ion-input
				placeholder=&quot;Email address&quot;
				formControlName=&quot;email&quot;
				autofocus=&quot;true&quot;
				clearInput=&quot;true&quot;
			&gt;&lt;/ion-input&gt;
		&lt;/ion-item&gt;
		&lt;div
			*ngIf=&quot;(email.dirty || email.touched) &amp;&amp; email.errors&quot;
			class=&quot;ion-margin-bottom input-errors&quot;
		&gt;
			&lt;span *ngIf=&quot;email.errors?.required&quot;&gt;Email is required&lt;/span&gt;
			&lt;span *ngIf=&quot;email.errors?.email&quot;&gt;Email is invalid&lt;/span&gt;
		&lt;/div&gt;

		&lt;ion-item&gt;
			&lt;ion-input
				placeholder=&quot;Password&quot;
				type=&quot;password&quot;
				formControlName=&quot;password&quot;
				clearInput=&quot;true&quot;
			&gt;&lt;/ion-input&gt;
		&lt;/ion-item&gt;
		&lt;div
			*ngIf=&quot;(password.dirty || password.touched) &amp;&amp; password.errors&quot;
			class=&quot;ion-margin-bottom input-errors&quot;
		&gt;
			&lt;span *ngIf=&quot;password.errors?.required&quot;&gt;Password is required&lt;/span&gt;
			&lt;span *ngIf=&quot;password.errors?.minlength&quot;&gt;Password needs to be 6 characters&lt;/span&gt;
		&lt;/div&gt;

		&lt;ion-button
			(click)=&quot;login()&quot;
			expand=&quot;full&quot;
			color=&quot;secondary&quot;
		&gt;Login&lt;/ion-button&gt;
		&lt;ion-button
			(click)=&quot;register()&quot;
			expand=&quot;full&quot;
		&gt;Register&lt;/ion-button&gt;
	&lt;/form&gt;
&lt;/ion-content&gt;</mark>
</code></pre>

### viii. Styling input error validation
<pre><code class="language-scss line-numbers"># src/app/pages/login/login.page.scss

.input-errors {
	font-size: small;
	color: #fff;
	background: var(--ion-color-danger);
	padding-left: 15px;
	padding-top: 5px;
	padding-bottom: 5px;
}
</code></pre>

### ix. Modify Router so it redirects to our login page instead of home.
<pre><code class="language-typescript line-numbers"># src/app/app-routing.module.ts

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule),
  },
  {
    path: '',
<mark>    redirectTo: 'login',</mark>
    pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
  },
];
</code></pre>

### x. How it should look like
<img src="./img/login.png" width="350px" alt="Screenshot of Login Page.">

## 4. Creating Home Page

### i. No need to create a new page. We will modify the existing `src/app/home` page.
The `Home` page is where our `Topics` will be listed. It is also used to display our `Logout` buttons.

### ii. Import required modules for `Home` page.
<pre><code class="language-typescript line-numbers"># src/app/pages/home/home.page.ts

<mark>import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';</mark>

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
<mark>  // Create sample topic as placeholder
  topics = [
    {
      id: 'ABCD1234',
      topic: 'SampleTopic'
    }
  ];</mark>


  constructor(
<mark>    private router: Router,
    private navController: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController,</mark>
  ) {}

  ngOnInit() {}
}
</code></pre>

### iii. Create logout function.
We will use Ionic's built in `Alert` prompt via `AlertController` to show a confirmation popup.  \
`LoadingController` is also used to show a loading popup during the process.


We will connect the logout function with the `AuthService` which we will create later.

<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

ngOnInit() {}

async logout() {
<mark>  const alert = await this.alertController.create({</mark>
    header: 'Confirm logout?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        cssClass: 'secondary'
      },
      {
        text: 'Yes',
<mark>        handler: async () => {
          const loading = await this.loadingController.create();
          loading.present();

          this.router.navigateByUrl('/', { replaceUrl: true });

          // TODO: Connect with AuthService
        }</mark>
      }
    ]
  });

<mark>  await alert.present();</mark>
}

// ...
</code></pre>

### iv. Create `Add Topic` function.
For `Add Topic`, we will use `AlertController` to create a prompt with a text input field for user to input their `Topic Name`.

<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async logout() {
  // ...
}

async addTopicPrompt() {
<mark>  const alert = await this.alertController.create({</mark>
    header: 'Add Topic',
<mark>    inputs: [
      {
        name: 'topic',
        type: 'text',
        placeholder: 'Topic Name',
        attributes: {
          required: true
        }
      },
    ],</mark>
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary'
      },
<mark>      {
        text: 'OK',
        handler: async (data) => {
          // Comment out these first, we're not using them yet.
          // const loading = await this.loadingController.create();
          // await loading.present();

          // TODO: Connect with TopicService
        }
      }</mark>
    ]
  });

<mark>  await alert.present();</mark>
}

// ...
</code></pre>

### v. Create `Edit Topic` function.
Same as `Add Topic`, we will use `AlertController` to display an input prompt to the user.

<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async addTopicPrompt() {
 // ...
}

async editTopicPrompt(topic) {
  const alert = await this.alertController.create({
    header: 'Edit Topic',
<mark>    inputs: [
      {
        name: 'topic',
        type: 'text',
        placeholder: 'Topic Name',
        value: topic.topic
      },
    ],</mark>
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary'
      },
<mark>      {
        text: 'OK',
        handler: async (data) => {
          // Comment out these first, we're not using them yet.
          // const loading = await this.loadingController.create();
          // await loading.present();
          
          // TODO: Connect to TopicService
        }
      }</mark>
    ]
  });

  await alert.present();
}

// ...
</code></pre>

### vi. Create `Delete Topic` function.
We will use `AlertController` to display confirmation dialog to user.

<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async editTopicPrompt() {
  // ...
}

async deleteTopic(id) {
  const alert = await this.alertController.create({
    header: 'Confirm delete?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        cssClass: 'secondary'
      },
<mark>      {
        text: 'Yes',
        handler: async() => {
          // const loading = await this.loadingController.create();
          // await loading.present();

          // TODO: Connect with TopicService
        }
      }</mark>
    ]
  });

  await alert.present();
}

// ...
</code></pre>

### vi. Create `View Topic` function.
We create the function first for later. For now, it will do nothing until we have both `TopicService` and `View Topic` page created.

<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async deleteTopic(id) {
  // ...
}

<mark>viewTopic() {
  // TODO: Go to View Topic Page
}</mark>

// ...
</code></pre>

### vii. Start working on the page design.
Just to make things look nicer.

<pre><code class="language-html line-numbers"># src/app/home/home.page.html

&lt;ion-header [translucent]=&quot;true&quot;&gt;
  &lt;ion-toolbar&gt;
    &lt;ion-title&gt;
<mark>      Home</mark>
    &lt;/ion-title&gt;
  &lt;/ion-toolbar&gt;
&lt;/ion-header&gt;

&lt;ion-content [fullscreen]=&quot;true&quot;&gt;&lt;/ion-content&gt;
</code></pre>

### viii. Add logout button in `ion-header`.
We will add the button in the `secondary` slot of the toolbar. In most devices, this slot is placed at the right side of the screen.

<pre><code class="language-html line-numbers"># src/app/home/home.page.html

&lt;ion-header [translucent]=&quot;true&quot;&gt;
  &lt;ion-toolbar&gt;
    &lt;ion-title&gt;
      Home
    &lt;/ion-title&gt;
<mark>    &lt;ion-buttons slot=&quot;secondary&quot;&gt;
      &lt;ion-button color=&quot;primary&quot; fill=&quot;clear&quot; (click)=&quot;logout()&quot;&gt;
        &lt;ion-icon slot=&quot;icon-only&quot; name=&quot;log-out-outline&quot;&gt;&lt;/ion-icon&gt;
      &lt;/ion-button&gt;
    &lt;/ion-buttons&gt;</mark>
  &lt;/ion-toolbar&gt;
&lt;/ion-header&gt;

&lt;ion-content [fullscreen]=&quot;true&quot;&gt;&lt;/ion-content&gt;
</code></pre>

### ix. Replace contents inside `ion-content`.
First, add `ion-fab` button for `Add Topic`.

<pre><code class="language-html line-numbers"># src/app/home/home.page.html

&lt;ion-header [translucent]=&quot;true&quot;&gt;
  &lt;!-- ... --&gt;
&lt;/ion-header&gt;

<mark>&lt;ion-content [fullscreen]=&quot;true&quot;&gt;
  &lt;ion-fab vertical=&quot;bottom&quot; horizontal=&quot;end&quot; slot=&quot;fixed&quot;&gt;
    &lt;ion-fab-button fill=&quot;clear&quot; (click)=&quot;addTopicPrompt()&quot;&gt;
      &lt;ion-icon name=&quot;add&quot;&gt;&lt;/ion-icon&gt;
    &lt;/ion-fab-button&gt;
  &lt;/ion-fab&gt;
&lt;/ion-content&gt;</mark>
</code></pre>

Next, create an `ion-list` to display `ion-card`. This will be used to display our `Topics` later on.
<pre><code class="language-html line-numbers"># src/app/home/home.page.html

&lt;ion-header [translucent]=&quot;true&quot;&gt;
  &lt;!-- ... --&gt;
&lt;/ion-header&gt;

&lt;ion-content [fullscreen]=&quot;true&quot;&gt;
  &lt;ion-fab vertical=&quot;bottom&quot; horizontal=&quot;end&quot; slot=&quot;fixed&quot;&gt;
    &lt;ion-fab-button fill=&quot;clear&quot; (click)=&quot;addTopicPrompt()&quot;&gt;
      &lt;ion-icon name=&quot;add&quot;&gt;&lt;/ion-icon&gt;
    &lt;/ion-fab-button&gt;
  &lt;/ion-fab&gt;

<mark>  &lt;ion-list *ngFor=&quot;let topic of topics&quot;&gt;
    &lt;ion-card&gt;
      &lt;ion-item&gt;
        &lt;ion-label&gt;Topic Name&lt;/ion-label&gt;
        &lt;ion-button fill=&quot;clear&quot; slot=&quot;end&quot; (click)=&quot;viewTopic(topic)&quot;&gt;
          &lt;ion-icon slot=&quot;icon-only&quot; name=&quot;stats-chart-outline&quot;&gt;&lt;/ion-icon&gt;
        &lt;/ion-button&gt;
        &lt;ion-button fill=&quot;clear&quot; slot=&quot;end&quot; (click)=&quot;editTopicPrompt(topic)&quot;&gt;
          &lt;ion-icon slot=&quot;icon-only&quot; name=&quot;create-outline&quot;&gt;&lt;/ion-icon&gt;
        &lt;/ion-button&gt;
        &lt;ion-button fill=&quot;clear&quot; slot=&quot;end&quot; (click)=&quot;deleteTopic(topic.id)&quot;&gt;
          &lt;ion-icon slot=&quot;icon-only&quot; name=&quot;close-circle-outline&quot;&gt;&lt;/ion-icon&gt;
        &lt;/ion-button&gt;
      &lt;/ion-item&gt;
    &lt;/ion-card&gt;
  &lt;/ion-list&gt;</mark>
&lt;/ion-content&gt;
</code></pre>

### x. How it should look like
| | | |
| --- | --- | --- |
| ![Home Page](img/home.png) | ![Logout](img/logout.png) | |
| ![Add Topic](img/add-topic.png) | ![Edit Topic](img/edit-topic.png) | ![Delete Topic](img/delete-topic.png) |
| | | |

## 5. Create View Topic Page

### i. Generate page
> `ionic g page pages/ViewTopic`

### ii. Import `ChartsModule` into `view-topic.module.ts`.
<pre><code class="language-typescript line-numbers"># src/app/pages/view-topic/view-topic.module.ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewTopicPageRoutingModule } from './view-topic-routing.module';

import { ViewTopicPage } from './view-topic.page';
<mark>import { ChartsModule } from '@rinminase/ng-charts';</mark>

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewTopicPageRoutingModule,
<mark>    ChartsModule,</mark>
  ],
  declarations: [ViewTopicPage]
})
export class ViewTopicPageModule {}
</code></pre>

### iii. Import required modules into `view-topic.page.ts`.
<pre><code class="language-typescript line-numbers"># src/app/pages/view-topic/view-topic.page.ts

import { Component, OnInit } from '@angular/core';
<mark>import { ActivatedRoute } from '@angular/router';
import { ChartColor } from '@rinminase/ng-charts';
import { IMqttMessage, MqttService } from 'ngx-mqtt';</mark>
</code></pre>

### iv. Initialise `Topic` object, `Charts` data and get `params` from `router`.
<pre><code class="language-typescript line-numbers"># src/app/pages/view-topic/view-topic.page.ts

// ...

export class ViewTopicPage implements OnInit {
<mark>  topic = null;</mark>

<mark>  // Charts
  chartData = [
    { data: [], label: 'Value' }
  ];

  chartLabels = [];

  chartOptions = {
    responsive: true,
  };

  chartColors: ChartColor = [
    {
      borderColor: 'red',
      backgroundColor: 'rgba(255,0,0,0.3)',
    }
  ];

  chartLegend = true;</mark>

<mark>  constructor(
    private route: ActivatedRoute,
  ) {
    this.route.queryParams.subscribe((params) => {
      this.topic = params;

      // TODO: Connect with MqttService
    });
  }</mark>

  ngOnInit() {}
}
</code></pre>

### v. Create `updateChart()` function
<pre><code class="language-typescript line-numbers"># src/app/pages/view-topic/view-topic.page.ts

// ...

export class ViewTopicPage implements OnInit {

  // ...

  ngOnInit() {}

  // ...

<mark>  updateChart(value) {
    const now = new Date();
    this.chartLabels.push(`${now.getHours()}:${now.getMinutes()}:${now.getSeconds()}`);
    this.chartData[0].data.push(value);
  }</mark>
}
</code></pre>

### vi. Add back button to `view-topic.page.html`.
<pre><code class="language-html line-numbers"># src/app/pages/view-topic/view-topic.page.html

&lt;ion-header&gt;
	&lt;ion-toolbar&gt;
<mark>		&lt;ion-buttons slot=&quot;start&quot;&gt;
			&lt;ion-back-button&gt;&lt;/ion-back-button&gt;
		&lt;/ion-buttons&gt;
		&lt;ion-title&gt;{{ topic?.topic }}&lt;/ion-title&gt;</mark>
	&lt;/ion-toolbar&gt;
&lt;/ion-header&gt;
</code></pre>

### vi. Add `Line Chart` to `view-topic.page.html`.
<pre><code class="language-html line-numbers"># src/app/pages/view-topic/view-topic.page.html

&lt;ion-header&gt;
	&lt;ion-toolbar&gt;
		&lt;ion-buttons slot=&quot;start&quot;&gt;
			&lt;ion-back-button&gt;&lt;/ion-back-button&gt;
		&lt;/ion-buttons&gt;
		&lt;ion-title&gt;{{ topic?.topic }}&lt;/ion-title&gt;
	&lt;/ion-toolbar&gt;
&lt;/ion-header&gt;

<mark>&lt;ion-content class=&quot;ion-padding&quot;&gt;
	&lt;div style=&quot;display: block;&quot;&gt;
		&lt;canvas
			baseChart
			width=&quot;400&quot;
			height=&quot;400&quot;
			chartType=&quot;line&quot;
			[datasets]=&quot;chartData&quot;
			[labels]=&quot;chartLabels&quot;
			[options]=&quot;chartOptions&quot;
			[colors]=&quot;chartColors&quot;
			[legend]=&quot;chartLegend&quot;
			[plugins]=&quot;chartPlugins&quot;
		&gt;&lt;/canvas&gt;
	&lt;/div&gt;
&lt;/ion-content&gt;</mark>
</code></pre>

### vii. How it should look like
<img src="./img/view-topic.png" alt="View Topic page screenshot." width="350px">

## 6. Setup Firebase

### i. Create a Firebase Project

1. Go to the Firebase Console: https://console.firebase.google.com/u/0/  \
   ![Firebase Console](./img/firebase/1.jpg)
2. Click **`"Add Project"`**.
3. Enter project name. It can be anything. For this tutorial, we will use _`"iotmonitor"`_.  \
   ![Firebase project name](./img/firebase/2.jpg)
4. Click **`"Continue"`**.
5. Disable **`"Google Analytics"`**.  \
   ![Disable Google Analytics](./img/firebase/3.jpg)
6. Click **`"Create Project"`**.

### ii. Enable `Authentication` provider.

1. Click **`"Authentication"`** on the sidemenu.
2. Click **`"Get Started"`**.  \
   ![Authentication Get Started](./img/firebase/4.jpg)
3. Enable **`"Email/Password"`** sign-in provider.  \
   ![Sign-in Provider](./img/firebase/5.jpg)
4. Click **`"Save"`**.

### iii. Enable `Firestore Database`.

1. Click **`"Firestore Database"`** on the sidemenu.
2. Click **`"Create Database"`**.  \
   ![Firestore Get Started](./img/firebase/6.jpg)
3. Chooose **`"Start in test mode"`**.  \
   ![Firestore mode](./img/firebase/7.jpg)
4. Click **`"Next"`**.
5. Choose **`"asia-southeast1"`** in **`"Cloud Firestore location"`** dropdown menu.  \
   ![Firestore location](./img/firebase/8.jpg)
6. Click **`"Enable"`**.
### iv. Create Firebase Config for web app.
1. Click **`"Project Overview"`** on the sidemenu.
2. Click the **`"Web"`** icon to create a Firebase config file for web-based applications.  \
   ![Firestore location](./img/firebase/10.jpg)
3. Type anything into **`"App Nickname"`**.  \
   ![Nickname](./img/firebase/11.jpg)
4. Click **`"Register App"`**.
5. Copy contents of **`"firebaseConfig"` ONLY**.  \
   ![Firebase config](./img/firebase/12.jpg)

### v. Add `firebaseConfig` to `src/environments/environment.ts`
<pre><code class="language-typescript line-numbers"># src/environments/environment.ts

// ...

export const environment = {
  production: false,
<mark>  firebaseConfig: {
    // PASTE CONTENTS OF firebaseConfig
    // THAT YOU GET FROM THE FIREBASE CONSOLE HERE
  }</mark>
};

// ...
</code></pre>

### vi. Import `Firebase` and `AngularFire` modules to `app.module.ts`
<pre><code class="language-typescript line-numbers"># src/app/app.module.ts

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

<mark>import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';</mark>

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
<mark>    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,</mark>
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
</code></pre>

## 7. Setup MQTT

We will use MQTT protocol in this app to monitor messages send from our "IOT devices".  \
For this tutorial, we will be simulating the MQTT protocol via an online MQTT client.

### i. Get an MQTT Broker account/server
For this tutorial we will use a free public MQTT 5 broker by EMQX. No need to setup our own broker server or accounts. This is only for testing purposes.

1. Go to https://www.emqx.com/en/mqtt/public-mqtt5-broker to get a free MQTT broker. No signup required.
2. Take note values from **`"Access Information"`** table.  \
   ![MQTT Broker](img/mqtt/1.jpg)

### ii. Add `mqttConfig` into `environment.ts`
<pre><code class="language-typescript line-numbers"># src/environments/environment.ts

// ...

export const environment = {
  production: false,
  firebaseConfig: {
    // ... 
  }
<mark>  mqttConfig: {
    hostname: 'broker.emqx.io',
    port: 8083,
    path: '/mqtt'
  }</mark>
};

// ...
</code></pre>

### iii. Import `ngx-mqtt` modules to `app.module.ts`
<pre><code class="language-typescript line-numbers"># src/app/app.module.ts

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

<mark>import { IMqttServiceOptions, MqttModule } from 'ngx-mqtt';</mark>

<mark>const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: environment.mqttConfig.hostname,
  port: environment.mqttConfig.port,
  path: environment.mqttConfig.path,
};</mark>

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
<mark>    MqttModule.forRoot(MQTT_SERVICE_OPTIONS)</mark>
  ],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule {}
</code></pre>

### iv. Add `MqttService` into `view-topic.page.ts`
<pre><code class="language-typescript line-numbers"># src/app/pages/view-topic/view-topic.page.ts

// ...

export class ViewTopicPage implements OnInit {

  // ...

  constructor(
    private route: ActivatedRoute,
<mark>    private _mqttService: MqttService</mark>
  ) {
    this.route.queryParams.subscribe((params) => {
      this.topic = params;

<mark>      this._mqttService.observe(this.topic.topic).subscribe((message: IMqttMessage) => {
        this.updateChart(message.payload.toString());
      });</mark>
    });
  }

  // ...

}
</code></pre>

## 8. Create `AuthService` Service
This is where we will create our Authentication service. We will connect our application to Firebase Firestore and store user data in there. We will also make use of Firebase Authentication provider to create a simple registration/login/logout flow for the application.

### i. Generate service
> `ionic g service services/auth/auth`

### ii. Import `AngularFireAuth` and `AngularFirestore`
<pre><code class="language-typescript line-numbers"># src/app/services/auth/auth.service.ts

import { Injectable } from '@angular/core';
<mark>import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';</mark>

@Injectable({
  providedIn: 'root'
})
export class AuthService {

<mark>  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) {}
}</mark>
</code></pre>

### iii. Create a `User` interface and let `AngularFireAuth` observe the change
We will create a `User` interface as a template for the `User` model that we will be using in this application.

Then, we will tell `AngularFireAuth` to observe the changes to the currently authenticated user so that it will update the `currentUser` object in the service for the application to use.

<pre><code class="language-typescript line-numbers"># src/app/services/auth/auth.service.ts

import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

<mark>export interface User {
  uid: string;
  email: string;
};</mark>

@Injectable({
  providedIn: 'root'
})
export class AuthService {
<mark>  currentUser: User = null;</mark>

  constructor(
    private afAuth: AngularFireAuth,
    private afs: AngularFirestore
  ) {
<mark>    this.afAuth.onAuthStateChanged((user) => {
      this.currentUser = user;
    });</mark>
  }
}
</code></pre>

### iv. Create `register()` function
This function make use of the `createUserWithEmailAndPassword()` function by `AngularFireAuth` to create a simple registration process with only email and password.


After the `User` is registered, we will save it into a `Collection` called `users` in our `Firestore` database. This is only used for us to get a list of registered users. Password is NOT stored in this `Collection`, that is handled completely by `AngularFireAuth`.

<pre><code class="language-typescript line-numbers"># src/app/services/auth/auth.service.ts

export class AuthService {

  // ...

<mark>  async register({email, password}): Promise&lt;any&gt; {
    const credential = await this.afAuth.createUserWithEmailAndPassword(email, password);

    return this.afs
      .doc(`users/$uid`)
      .set({
        uid: credential.user.uid,
        email: credential.user.email
      });
  }</mark>
}
</code></pre>

### v. Create `login()` function
This function make use of the `signInWithEmailAndPassword()` function by `AngularFireAuth` to create a simple login process.

<pre><code class="language-typescript line-numbers"># src/app/services/auth/auth.service.ts

export class AuthService {

  // ...

  async register({email, password}): Promise&lt;any&gt; {
    // ...
  }

<mark>  login({email, password}) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }</mark>
}
</code></pre>

### vi. Create `logout()` function
This function make use of the `signOut()` function by `AngularFireAuth` to create a simple logout process.

<pre><code class="language-typescript line-numbers"># src/app/services/auth/auth.service.ts

export class AuthService {

  // ...

  login({email, password}) {
    // ...
  }

<mark>  logout(): Promise&lt;void&gt; {
    return this.afAuth.signOut();
  }</mark>
}
</code></pre>

### vii. Import `AuthService` in `login.page.ts`
<pre><code class="language-typescript line-numbers"># src/app/pages/login.page.ts

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
<mark>import { AuthService } from 'src/app/services/auth/auth.service';</mark>

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  credentialForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private alertController: AlertController,
    private loadingController: LoadingController,
<mark>    private authService: AuthService</mark>
  ) { }

  ngOnInit() {
    // ...
  }
}
</code></pre>

### viii. Connect `register()` in `login.page.ts` with `AuthService`
<pre><code class="language-typescript line-numbers"># src/app/pages/login.page.ts

// ...

ngOnInit() {
  // ...
}

async register() {
<mark>  const loading = await this.loadingController.create();
  await loading.present();
  
  this.authService
      .register(this.credentialForm.value)
      .then((user) =&gt; {
        loading.dismiss();
        this.router.navigateByUrl(&apos;/home&apos;, { replaceUrl: true });
      }, async (error) =&gt; {
        loading.dismiss();

        const alert = await this.alertController.create({
          header: &apos;Register Failed&apos;,
          message: error.message,
          buttons: [&apos;OK&apos;]
        });

        await alert.present();
      });</mark>
}

// ...
</code></pre>

### ix. Connect `login()` in `login.page.ts` with `AuthService`
<pre><code class="language-typescript line-numbers"># src/app/pages/login.page.ts

// ...

async register() {
  // ...
}

async login() {
<mark>  const loading = await this.loadingController.create();
  await loading.present();
  
  this.authService
      .login(this.credentialForm.value)
      .then((user) =&gt; {
        loading.dismiss();
        this.router.navigateByUrl(&apos;/home&apos;, { replaceUrl: true });
      }, async (error) =&gt; {
        loading.dismiss();

        const alert = await this.alertController.create({
          header: &apos;Login Failed&apos;,
          message: error.message,
          buttons: [&apos;OK&apos;]
        });

        await alert.present();
      });</mark>
}

// ...
</code></pre>

### x. Import `AuthService` in `home.page.ts`
<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
<mark>import { AuthService } from '../services/auth/auth.service';</mark>

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  topics = [];

  constructor(
    private router: Router,
    private navController: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController,
<mark>    private authService: AuthService,</mark>
  ) {}

  // ...
}
</code></pre>

### xi. Connect `logout()` in `home.page.ts` with `AuthService`
<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

ngOnInit() {}

async logout() {
  const alert = await this.alertController.create({
    header: 'Confirm logout?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        cssClass: 'secondary'
      },
      {
        text: 'Yes',
<mark>        handler: async () => {
          const loading = await this.loadingController.create();
          loading.present();

          this.authService.logout()
            .then(() => {
              loading.dismiss();
              this.router.navigateByUrl('/', { replaceUrl: true });
            });
        }</mark>
      }
    ]
  });

<mark>  await alert.present();</mark>
}

// ...
</code></pre>

## 9. Create `TopicService` Service

### i. Generate service
> `ionic g service services/topic/topic`

### ii. Import modules
<pre><code class="language-typescript line-numbers"># src/app/services/topic/topic.service.ts

import { Injectable } from '@angular/core';
<mark>import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';</mark>

<mark>export interface Topic {
  id: string;
  topic: string;
  createdAt: firebase.default.firestore.FieldValue;
};</mark>

@Injectable({
  providedIn: 'root'
})
export class TopicService {
<mark>  private _currentTopic: Topic = null;</mark>

  constructor(
<mark>    private afs: AngularFirestore,</mark>
  ) { }
}
</code></pre>

### iii. Create `addTopic()` function
<pre><code class="language-typescript line-numbers"># src/app/services/topic/topic.service.ts

export class TopicService {

  // ... constructor

<mark>  addTopic(data) {
    return this.afs.collection('topics').add({
      topic: data.topic,
      createdAt: firebase.default.firestore.FieldValue.serverTimestamp()
    });
  }</mark>
}
</code></pre>

### iv. Create `updateTopic()` function
<pre><code class="language-typescript line-numbers"># src/app/services/topic/topic.service.ts

export class TopicService {

  // ...

  addTopic(data) {
    // ...
  }

<mark>  updateTopic(data) {
    return this.afs.collection(&apos;topics&apos;).doc(data.id)
      .update({
        topic: data.topic
      });
  }</mark>
}
</code></pre>

### v. Create `getTopics()` function
We will use `AngularFirestore` to get data from `Collection` called `'topics'`, sort it by the oldest first. Then, track the `Collection` with `.valueChanges()` and return it as an `Observable` object so our application can retrieve data from it in realtime.

<pre><code class="language-typescript line-numbers"># src/app/services/topic/topic.service.ts

export class TopicService {

  // ...

  updateTopic(data) {
    // ...
  }

<mark>  getTopics() {
    return this.afs.collection(&apos;topics&apos;, ref =&gt; ref.orderBy(&apos;createdAt&apos;))
      .valueChanges({ idField: &apos;id&apos; }) as Observable&lt;Topic[]&gt;;
  }</mark>
}
</code></pre>

### vi. Create `getTopic()` function
Get only one `Document` inside a `Firestore Collection` by `ID`.

<pre><code class="language-typescript line-numbers"># src/app/services/topic/topic.service.ts

export class TopicService {

  // ...

  getTopics() {
    // ...
  }

<mark>  getTopic(id) {
    return this.afs.collection(&apos;topics&apos;).doc(id).get();
  }</mark>
}
</code></pre>

### vii. Create `deleteTopic()` function
Delete `Document` inside a `Firestore Collection` by `ID`.

<pre><code class="language-typescript line-numbers"># src/app/services/topic/topic.service.ts

export class TopicService {

  // ...

  getTopic(id) {
    // ...
  }

<mark>  deleteTopic(id) {
    return this.afs.collection(&apos;topics&apos;).doc(id).delete();
  }</mark>
}
</code></pre>

### viii. Create getter and setter functions
Delete `Document` inside a `Firestore Collection` by `ID`.

<pre><code class="language-typescript line-numbers"># src/app/services/topic/topic.service.ts

export class TopicService {

  // ...

  deleteTopic(id) {
    // ...
  }

<mark>  public set currentTopic(topic) {
    this._currentTopic = topic;
  }

  public get currentTopic() {
    return this._currentTopic;
  }</mark>
}
</code></pre>

### x. Import `TopicService` into `home.page.ts`
<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth/auth.service';
<mark>import { TopicService } from '../services/topic/topic.service';</mark>

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
<mark>  topics = []; // IMPORTANT: CHANGE THIS LINE BECAUSE WE DO NOT WANT TO USE SAMPLE DATA ANYMORE</mark>

  constructor(
    private router: Router,
    private navController: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private authService: AuthService,
<mark>    private topicService: TopicService,</mark>
  ) {}

  // ...
}
</code></pre>

### xi. Call `getTopics()` in `ngOnInit()` in `home.page.ts`
<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

export class HomePage implements OnInit {
  topics = [];

  constructor(
    private router: Router,
    private navController: NavController,
    private loadingController: LoadingController,
    private alertController: AlertController,
    private authService: AuthService,
    private topicService: TopicService,
  ) {}

<mark>  async ngOnInit() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.topicService
      .getTopics()
      .subscribe((response) =&gt; {
        this.topics = response;
        loading.dismiss();
      }, async (error) =&gt; {
        loading.dismiss();

        const alert = await this.alertController.create({
          header: &apos;Loading Topics Failed&apos;,
          message: error.message,
          buttons: [&apos;OK&apos;]
        });

        await alert.present();
      });
  }</mark>
}

// ...

</code></pre>

### xii. Connect `addTopicPrompt()` in `home.page.ts` with `TopicService`
<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async logout() {
  // ...
}

async addTopicPrompt() {
  const alert = await this.alertController.create({
    header: 'Add Topic',
    inputs: [
      {
        name: 'topic',
        type: 'text',
        placeholder: 'Topic Name',
        attributes: {
          required: true
        }
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary'
      },
<mark>      {
        text: 'OK',
        handler: async (data) => {
          const loading = await this.loadingController.create();
          await loading.present();
          
          this.topicService.addTopic(data)
            .then(async () => {
              loading.dismiss();
            }, async (error) => {
              loading.dismiss();
      
              const alert = await this.alertController.create({
                header: 'Add Topic Failed',
                message: error.message,
                buttons: ['OK']
              });
      
              await alert.present();
            });
        }
      }</mark>
    ]
  });

  await alert.present();
}

// ...
</code></pre>

### xiii. Connect `editTopicPrompt()` in `home.page.ts` with `TopicService`
<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async addTopicPrompt() {
 // ...
}

async editTopicPrompt(topic) {
  const alert = await this.alertController.create({
    header: 'Edit Topic',
    inputs: [
      {
        name: 'topic',
        type: 'text',
        placeholder: 'Topic Name',
        value: topic.topic
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary'
      },
<mark>      {
        text: 'OK',
        handler: async (data) => {
          const loading = await this.loadingController.create();
          await loading.present();

          this.topicService.updateTopic({ id: topic.id, topic: data.topic })
            .then(async () =&gt; {
              loading.dismiss();
            }, async (error) =&gt; {
              loading.dismiss();
      
              const alert = await this.alertController.create({
                header: &apos;Add Topic Failed&apos;,
                message: error.message,
                buttons: [&apos;OK&apos;]
              });
      
              await alert.present();
            });
        }
      }</mark>
    ]
  });

  await alert.present();
}

// ...
</code></pre>

### xiv. Connect `deleteTopic()` in `home.page.ts` with `TopicService`
We will use `AlertController` to display confirmation dialog to user.

<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async editTopicPrompt() {
  // ...
}

async deleteTopic(id) {
  const alert = await this.alertController.create({
    header: 'Confirm delete?',
    buttons: [
      {
        text: 'No',
        role: 'cancel',
        cssClass: 'secondary'
      },
<mark>      {
        text: 'Yes',
        handler: async() => {
          const loading = await this.loadingController.create();
          await loading.present();

          this.topicService
            .deleteTopic(id)
            .then(async (response) =&gt; {
              loading.dismiss();

              const alert = await this.alertController.create({
                header: &apos;Topic Deleted&apos;,
                buttons: [&apos;OK&apos;]
              });

              await alert.present();
            }, async (error) =&gt; {
              loading.dismiss();

              const alert = await this.alertController.create({
                header: &apos;Loading Topics Failed&apos;,
                message: error.message,
                buttons: [&apos;OK&apos;]
              });

              await alert.present();
            });
        }
      }</mark>
    ]
  });

  await alert.present();
}

// ...
</code></pre>

### xv. Connect `viewTopic()` in `home.page.ts` with `TopicService`
<pre><code class="language-typescript line-numbers"># src/app/home/home.page.ts

// ...

async deleteTopic(id) {
  // ...
}

<mark>viewTopic() {
  const params = Object.assign({}, topic);
  this.navController.navigateForward(&apos;/view-topic&apos;, { queryParams: params });
}</mark>

// ...
</code></pre>

## 10. Send Message From MQTT Client

### i. Create a Topic
1. Open your newly finished app.
2. Login or Register.
3. Click the **`"+"`** button on the bottom right to add a new `Topic`.
4. Type anything as the `Topic Name`. You will need to remember this.
5. Click **`"OK"`**. Wait for `Topic` to appear in the list.
6. Click the charts icon on the right side of the `Topic` name to view the `Topic`.
7. Stay on that page. Here we will observe the chart getting updated as we seen new `Messages` from our MQTT Client later.

### ii. MQTT WebSocket Toolkit by EMQX
1. Open MQTT WebSocket Toolkit: [http://tools.emqx.io/](http://tools.emqx.io/)
2. Input anything into **`"Name"`**. No need to change anything else. For example, I type **`"hakimz"`** as my username and did not change anything else.  \
   ![MQTT WebSocket Toolkit](./img/mqtt/2.jpg)
3. Click **`"Connect"`**.
4. Look at the textarea in the bottom right. This is where we will broadcast messages to our application.
5. For this tutorial, we will send a **`"Payload"`** type of **`"Plaintext"`**.
6. Make sure to type the `Topic` name in the box provided. Use the `Topic` name that you have created previously in the application.
7. For the value, type a random integer. Our simple application will only accept integers as messages for now and we only want to plot the numbers on the charts.
   ![Broadcast MQTT](./img/mqtt/4.jpg)
8. Click the `paper plane` icon to broadcast the message.
9. Go back to your application and watch the graph gets plotted.  \
    <img src="./img/view-topic-plotted.png" width="350px">
10. You can send as many messages as you want. Try randomising the numbers so you can see better the graph plot.

## What's Next For You?

You have successfully created a simple Ionic mobile application which leverages the powerful Google Firebase platform and MQTT protocol. From this point forward, you may use the knowledge you have gained so far to implement these modules and apply the techniques you've learned here on your own mobile application.

Using the MQTT protocol, you can send data from your IOT devices to your mobile application and vice versa. You can also apply the Firestore services to save the Messages obtained from the MQTT broadcasts.

<!-- Scripts -->
<script src="./prism.js"></script>